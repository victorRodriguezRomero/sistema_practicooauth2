﻿using AutoMapper;
using DatingApp.Api.Data;
using DatingApp.Api.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.Api.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class UsersController:ControllerBase
    {
        private readonly IDatingRepository _repo;
        private readonly IMapper _mapper;

        public UsersController( IDatingRepository repo, IMapper mapper )
        {
            _repo = repo;
            _mapper = mapper;
        }
        [HttpGet]
        
        public async Task<IActionResult> GetUsers()
        {
            var users = await _repo.GetUsers();
            var UsersToReturn = _mapper.Map<IEnumerable<UserForListDto>>(users);
            return Ok(UsersToReturn);
        }
        
        [HttpGet("{id}", Name = "GetUser")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user = await _repo.GetUser(id);
            var UserToReturn = _mapper.Map<UserDatailedDto>(user);
            return Ok(UserToReturn);
        }
    }
}

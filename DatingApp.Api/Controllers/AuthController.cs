﻿using AutoMapper;
using DatingApp.Api.Data;
using DatingApp.Api.Dtos;
using DatingApp.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DatingApp.Api.Controllers
{
  //  [Authorize]
  [AllowAnonymous]
    [Route("api/[controller]")]
   //[ApiController]
    
    public class AuthController:ControllerBase
    {
 

        private readonly IConfiguration _conf;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _Mapper;

        public AuthController ( IConfiguration conf, 
            UserManager<User> userManager,
            SignInManager<User> signInManager, IMapper mapper)
        {
            _conf = conf;
            _userManager = userManager;
            _signInManager = signInManager;
            _Mapper = mapper;
           
        }

        [HttpPost("register")]

        public async Task<IActionResult> Register([FromBody]UserForRegisterDto userForRegisterDto)
        {
            var userToCreate = _Mapper.Map<User>(userForRegisterDto);

            var result = await _userManager.CreateAsync(userToCreate, userForRegisterDto.Password);

            var userReturn = _Mapper.Map<UserDatailedDto>(userToCreate);

            if (result.Succeeded)
            {

                return CreatedAtRoute("GetUser",
                    new { controller = "Users", id = userToCreate.Id }, userReturn);

            }
            return BadRequest(result.Errors);
        }

        [HttpPost ("login")]
        public async Task<IActionResult>login([FromBody]UserForLoginDto userForloginDto)
        {

            var user = await _userManager.FindByNameAsync(userForloginDto.Username);

            var result = await _signInManager
                .CheckPasswordSignInAsync(user, userForloginDto.password, false);

            if (result.Succeeded)
            {
                var appUser = await _userManager.Users.Include(p => p.Photos)
                    .FirstOrDefaultAsync(u => u.NormalizedUserName == userForloginDto.Username.ToUpper());
                var userToReturn = _Mapper.Map<UserForListDto>(appUser);

                return Ok(new
                {
                    token = GenerateToken(appUser),
                    User = userToReturn
                });
            }
            return Unauthorized();
        }

        private async Task<string> GenerateToken(User user)
        {
            var claims = new List<Claim>
              {
             new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
             new Claim(ClaimTypes.Name, user.UserName)
            };

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(_conf.GetSection("Appsettings:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        private IActionResult StatusCode(int v)
        {
            throw new NotImplementedException();
        }

        private IActionResult BadRequest(string v)
        {
            throw new NotImplementedException();
        }

    }

    
}

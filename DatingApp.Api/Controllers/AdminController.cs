﻿using DatingApp.Api.Data;
using DatingApp.Api.Dtos;
using DatingApp.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController: ControllerBase
    {
        private readonly DataContext _context;
        private readonly UserManager<User> _userManager;

        public AdminController(DataContext context, UserManager<User> userManager)
        {
            _context = context;
          _userManager = userManager;
        }

        [Authorize(Policy= "RequiredAdminRole")]

        [HttpGet("userWithRoles")]

        public async Task<IActionResult> GetuserWithRoles()
        {
            var userlist = await (from user in _context.Users
                                  orderby user.UserName
                                  select new
                                  {
                                      id = user.Id,
                                      userNamw = user.UserName,
                                      Roles = (from userRole in user.UserRoles
                                               join role in _context.Roles
                                               on userRole.RoleId
                                               equals role.Id
                                               select role.Name).ToString()}).ToListAsync();

            return Ok(userlist);
        }

        [Authorize(Policy = "RequiredPhotoRole")]

        [HttpGet("photosWithRoles")]

        public IActionResult GetphotosWithRoles()
        {
            return Ok("Only Photos can see");
        }

        [Authorize(Policy = "RequiredAdminRole")]
        [HttpPost("EditRoles/{userName}")]

        public  async Task<IActionResult> EditRoles(string userName, RoleEditDto roleEditDto)
        {
            var user = await _userManager.FindByNameAsync(userName);
            var userRoles = await _userManager.GetRolesAsync(user);

            var selectedRoles = roleEditDto.RolesNames;

            selectedRoles = selectedRoles ?? new string[] {};

            var result = await _userManager.AddToRolesAsync(user, selectedRoles.Except(userRoles));

            if (!result.Succeeded) 
                return BadRequest("Faided to Add to Roles");

            result = await _userManager.RemoveFromRolesAsync(user, userRoles.Except(selectedRoles));

            if (!result.Succeeded)
                return BadRequest("Faided to remove to Roles");

            return Ok(await _userManager.GetRolesAsync(user));
            
        }

    }
}

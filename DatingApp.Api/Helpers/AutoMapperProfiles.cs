﻿using AutoMapper;
using DatingApp.Api.Data;
using DatingApp.Api.Dtos;
using DatingApp.Api.Models;
using System.Linq;


namespace DatingApp.Api.Helpers
{
    public class AutoMapperProfiles: Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>()
                .ForMember(dest => dest.PhotoUrl, opt =>
                {
                    opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
                })
            .ForMember(dest => dest.Age, opt =>
            {
                opt.ResolveUsing(d => d.DateOFBirth.CalculeAge());
            });
            CreateMap<User, UserDatailedDto>()
            .ForMember(dest => dest.PhotosUrl, opt => {
                opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
            })
             .ForMember(dest => dest.Age, opt =>
             {
                 opt.ResolveUsing(d => d.DateOFBirth.CalculeAge());
             });
            CreateMap<Photo, PhotoForDatailedData>();
            CreateMap<UserForRegisterDto, User>();
        }
    }
}

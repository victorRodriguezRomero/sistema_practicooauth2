﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.Api.Models
{
    public class User:IdentityUser<int>
    {
        
        public string Gander{ get; set; }

        public DateTime DateOFBirth { get; set; }

        public string KnowAs { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastActive { get; set; }

        public string Introduction { get; set; }

        public string  LookingFor { get; set; }

        public string Interests { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public ICollection<Photo> Photos { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }


    }

    
}

﻿using DatingApp.Api.Data;
using DatingApp.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.Api.Dtos
{
    public class UserDatailedDto
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Gander { get; set; }

        public int Age { get; set; }

        public string KnowAs { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastActive { get; set; }

        public string Introduction { get; set; }

        public string LookingFor { get; set; }

        public string Interests { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string PhotosUrl { get; set; }

        public ICollection<PhotoForDatailedData> Photos { get; set; }
    }
}

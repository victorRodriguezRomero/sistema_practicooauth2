﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.Api.Dtos
{
    public class RoleEditDto
    {
        public string [] RolesNames { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.Api.Data
{
    public class PhotoForDatailedData
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public string Descripcion { get; set; }

        public DateTime DateAdded { get; set; }

        public bool IsMain { get; set; }
    }
}
